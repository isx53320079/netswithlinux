###ex1:

	dhclient -r --> liberamos la ip actual
	ip a f dev enp2s0 --> hacemos un flush i eliminamos todas las direcciones ips asociadas a una interfaz
	systemctl stop NetworkManager --> paramos el nNetworkManager
	
	ip a a 10.8.8.120/16 dev enp2s0
	ip a a 10.7.20.100/24 dev enp2s0 --> añadimos las ip
	
	ip a --> miramos que este todo este correcto
	
	ping 10.8.1.1
	ping 10.7.20.1 --> funcionan los dos pings
	

###ex2:
	
	ip a a 10.9.9.220/24 dev enp2s0 --> añadimos ip
	ip a --> miramos que este todo este correcto

	ip r a 10.6.6.0/24 via 10.9.9.1 --> hacemos la ruta estatica a 10.6.6.0/24
	ip r --> miramos que este todo este correcto
	
	
###ex3:

	ping 10.6.6.2 --> hacemos ping
	tshark -i enp2s0 -c 30 -w /tmp/captura_pingeo.pcap icmp --> la orden que usaria para capturar los pings
	
	tshark -i enp2s0 -c2 -w /tmp/ping.pcap icmp --> ordenes del examen 
	tshark -r /tmp/ping.pcap
	

###ex4:

	ip link set usb0 name usip --> cambiamos el nombre
	ip link set usb20 address 44:44:44:00:00:20 --> añadimos la direccion macc
	ip a a 10.5.5.120/24 dev enp2s0 --> añadimos ip
	ip link set usb20 up --> levantamos interfaz
	ip a --> miramos que este todo este correcto
	
	ping 10.5.5.1 --> funciona correctamente y aparezco en la pizarra
	

###ex5:
	
	ip a f dev enp2s0
	ip a f dev usb20
	dhclient enp2s0
	ip a a 10.5.5.1/16 dev usb20
	ip link set usb20 up
	echo 1 > /proc/sys/net/ipv4/ip_forward
	iptables -t nat -A POSTROUTING -o enp2s0 -j MASQUERADE
	
	ip r s
	default via 192.168.0.5 dev enp2s0 
	192.168.0.0/16 dev enp2s0  proto kernel  scope link  src 192.168.3.20
	
	iptables -t nat -L
	Chain PREROUTING (policy ACCEPT)
	target     prot opt source               destination         
	
	Chain INPUT (policy ACCEPT)
	target     prot opt source               destination         
	
	Chain OUTPUT (policy ACCEPT)
	target     prot opt source               destination         
	
	Chain POSTROUTING (policy ACCEPT)
	target     prot opt source               destination         
	MASQUERADE  all  --  anywhere             anywhere            
	MASQUERADE  all  --  anywhere             anywhere            
	MASQUERADE  all  --  anywhere             anywhere 


###ex6:
	
	mtr -n --report papua.go.id > /tmp/papua20.txt --> 	informe de saltos
	
	
###ex7:

	netstat -utlnpa | grep cups
	tcp        0      0 127.0.0.1:631           0.0.0.0:*               LISTEN      1375/cupsd          
	tcp6       0      0 ::1:631                 :::*                    LISTEN      1375/cupsd 

	netstat -utlnpa | grep firefox
	tcp        0      0 192.168.3.20:34004      216.58.211.206:443      ESTABLISHED 4629/firefox        
	tcp        0      0 192.168.3.20:36826      172.217.22.174:443      ESTABLISHED 4629/firefox        
	tcp        0      0 192.168.3.20:53886      216.58.201.134:443      ESTABLISHED 4629/firefox        
	tcp        0      0 192.168.3.20:33980      216.58.211.206:443      ESTABLISHED 4629/firefox        
	tcp        0      0 192.168.3.20:56510      216.58.214.164:443      ESTABLISHED 4629/firefox        
	tcp        0      0 192.168.3.20:34098      199.16.157.105:443      ESTABLISHED 4629/firefox        
	tcp        0      0 192.168.3.20:52428      216.58.201.130:443      ESTABLISHED 4629/firefox        
	tcp        0      0 192.168.3.20:54128      74.125.206.156:443      ESTABLISHED 4629/firefox        
	tcp        0      0 192.168.3.20:39574      216.58.214.170:443      ESTABLISHED 4629/firefox        
	tcp        0      0 192.168.3.20:33072      216.58.204.142:443      ESTABLISHED 4629/firefox        
	tcp        0      0 192.168.3.20:33990      216.58.211.206:443      ESTABLISHED 4629/firefox        
	tcp        0      0 192.168.3.20:34036      216.58.211.206:443      ESTABLISHED 4629/firefox        
	tcp        0      0 192.168.3.20:48170      216.58.211.195:443      ESTABLISHED 4629/firefox


###ex8:

	
