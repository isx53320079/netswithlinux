BRIDGES Y ROUTERS

##### 1. Conexionado, nombres de interfaces y direcciones MAC

Para esta práctica hay que interconectar los equipos de una fila con 2 latiguillos de red 
(sin usar las rosetas del aula) siguiendo el siguiente esquema:

```
FILA DE 3 EQUIPOS:
                              |usbB                          
   +--------+            +--------+            +--------+   
   |        |eth0    usbA|        |usbC    eth0|        |   
   | HOST A +------------+ HOST B +------------+ HOST C |   
   |        |            |        |            |        |   
   +--------+            +--------+            +--------+   
                             |eth0                          
```

El equipo del medio de la fila ha de tener conectadas 3 tarjetas usb-ethernet. Se han de 
renombrar estas tarjetas y cambiar las mac. Las direcciones mac de cada tarjeta han de ser 
distintas siguiendo la siguiente codificación:(AA:AA:AA:00:00:YY)

Siendo YY la numeración de la tarjeta USB que lleva en su pegatina.

	ip link set usb2 address AA:AA:AA:00:00:21
	ip link set usb1 address AA:AA:AA:00:00:20
	ip link set usb0 address AA:AA:AA:00:00:19

Hay que renombrar las tarjetas de red para que se llamen: usbA, usbB, usbC y eth0(la de la placa base)

	ip link set usb2 name usbC
	ip link set usb1 name usbB
	ip link set usb0 name usbA


Al final la orden ip link show ha de mostrar algo como lo siguiente:

	1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN mode DEFAULT group default qlen 1
		link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
	2: eth0: <BROADCAST,MULTICAST> mtu 1500 qdisc fq_codel state DOWN mode DEFAULT group default qlen 1000
		link/ether 94:de:80:8a:4a:54 brd ff:ff:ff:ff:ff:ff
	6: usbA: <BROADCAST,MULTICAST> mtu 1500 qdisc fq_codel state DOWN mode DEFAULT group default qlen 1000
		link/ether aa:aa:aa:00:00:19 brd ff:ff:ff:ff:ff:ff
	7: usbC: <BROADCAST,MULTICAST> mtu 1500 qdisc fq_codel state DOWN mode DEFAULT group default qlen 1000
		link/ether aa:aa:aa:00:00:20 brd ff:ff:ff:ff:ff:ff
	8: usbB: <BROADCAST,MULTICAST> mtu 1500 qdisc fq_codel state DOWN mode DEFAULT group default qlen 1000
		link/ether aa:aa:aa:00:00:21 brd ff:ff:ff:ff:ff:ff


##### 2. Conectar 3 equipos haciendo routing y saliendo a internet:

A. Configurar las ips en cada equpo según el esquema y hacer pings desde hostA a hostB y desde hostC a hostA

```
ESQUEMA CAPA 3
                       +---------+
    172.17.21.0/24   .1| ROUTER  |.1   172.17.19.0/24
   +-------------------+ HOST B  +--------------------+
        |.2        usbA|         |usbC        |.2
        |eth0          +---------+            |eth0
     +------+                              +------+
     |HOST A|                              |HOST C|
     +------+                              +------+
     
     ip a a 172.17.21.1/24 dev usbA
	 ip a a 172.17.19.1/24 dev usbC

                            
    ping hostA a hostB     
    (64 bytes from 172.17.21.1: icmp seq=1 [t]=64 time=1.17ms)
    
    ping hostC a hostB                
	(64 bytes from 172.17.19.1: icmp seq=1 [t]=64 time=1.13ms)
```

B. Activar el bit de forwarding y poner como default gateway al host B que hará de router entre las dos redes,
 listar las rutas en host B y verificar que se pueden hacer pings entre HOST A y HOST C
	
	gateway(puerta de enlace)--> ip r a default via 172.17.19.1
							    
							     ip r a 0.0.0.0/0 via 172.17.21.1   (0.0.0.0/0 es todo internet)

	router -->	echo 1 > /proc/sys/net/ipv4/ip_forward

	
C. Conectar eth0 del HOST B a la roseta del aula, pedir una ip dinámica con dhclient. Verificar que desde el 
HOST B se puede hacer ping al 8.8.8.8. 

	--> dhclient enp2s0
	
	--> ping 8.8.8.8
		PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
		64 bytes from 8.8.8.8: icmp_seq=1 ttl=55 time=11.8 ms
		64 bytes from 8.8.8.8: icmp_seq=2 ttl=55 time=11.6 ms

D. Activar el enmascarmiento de ips para el tráfico saliente por eth0 y verificar que HOST A Y HOST C pueden
 hacer ping al 8.8.8.8

	--> B)iptables -t nat -A POSTROUTING -o enp2s0 -j MASQUERADE.
	
	--> pueden hacer ping al 8.8.8.8

E. Poner como servidor dns a 8.8.8.8 en /etc/resolv.conf en hostA y host C y verificar que se puede navegar 
por internet con  haciendo un wget de http://www.netfilter.org

	--> A y C)echo "nameserver 8.8.8.8" > /etc/reslov.conf
	
	--> pueden navegar por internet

##### 3. Conectar 2 equipos utilizando un equipo intermedio que hace de bridge. 

En esta práctica queremos que HOST B trabaje como si fuera un switch, interconectando a host A y host C. 

A. Hacer flush de todas las ips y verificar que no queda ninguna ruta ni dirección ip asociada a ningún equipo.
	
	systemctl status firewalld
	systemctl status NetworkManager
	systemctl stop NetworkManager
	ip r f all
	ip a f enp2s0
	dhclient -r

B. Crear el bridge br0 y añadir usbA y usbC a ese bridge. Listar con detalle la configuración del bridge.

	
	brctl
	brctl show
	brctl addbr br0
	brctl addif br0 usbA
	brctl addif br0 usbc

C. En el host A nos ponemos la ip 192.168.100.A/24 y en el equipo C la ip 192.168.100.B/24. Hacemos ping entre
 los dos equipos y debería de ir.
 
	ip a a 192.168.100.19
	ip a a 192.168.100.21
	funcona los dos pueden hacerse ping entre ellos

D. Listar en host B la tabla de relación de puertos y MACs en el bridge

	ip link set usbA address AA:AA:AA:00:00:21
	ip link set usbC address AA:AA:AA:00:00:19

##### 4. Conectar todos los equipos del aula haciendo switching.

Conseguir esta interconexión entre las filas:
```
\
                   |                     
                   +------+              
                          |              
  BRIDGE : br0            |              
 +-------------------------------------+ 
 | +---+  +---+  +---+  +---+          | 
 | | 1 |  | 2 |  | 3 |  | 4 |   HOST B | 
 | +---+  +---+  +---+  +---+          | 
 |   |usbA  |usbC  |eth0   usbB        | 
 +-------------------------------------+ 
     |      |      |                     
     |      |      |                     
     |      |      |                     
 +---+--+ +-+----+ |                     
 |HOST A| |HOST C| +------+              
 +------+ +------+        |              
                          |              
                          |              
  BRIDGE : br0            |              
 +-------------------------------------+ 
 | +---+  +---+  +---+  +---+          | 
 | | 1 |  | 2 |  | 3 |  | 4 |   HOST B | 
 | +---+  +---+  +---+  +---+          | 
 |   |usbA  |usbC  |eth0   usbB        | 
 +-------------------------------------+ 
     |      |      |                     
     |      |      |                     
     |      |      |                     
 +---+--+ +-+----+ |                     
 |HOST A| |HOST C| +------+
 +------+ +------+        |
                          |
                          +
```
Cada equipo ha de tener una ip 192.168.100.B/24

	ip link set usbB address AA:AA:AA:00:00:20
	ip link set usbB up
	brctl addif br0 usbB

A. Lanzar fpings para verificar que todos los equipos A y C responden


B. Asignar una ip al br0 del hostB y lanzar fping para verificar que todos los equipos A,B y C responden

C. Listar la tabla de asignación de puertos y MACs después de hacer un fping 

##### 5. Conectar todos los equipos del aula haciendo routing.

```
\
                         +
                         |
                         |
                         |
                     +---+-----+
  172.17.XX.0/24   .1| ROUTER  |.1   172.17.XX.0/24
 +-------------------+ HOST B  +--------------------+
      |.2            |         |          .2|
      |              +---------+            |
   +--+---+              |.1             +--+---+
   |HOST A|              |               |HOST C|
   +------+              |               +------+
                         |
                         |
                         |172.16.XX.0/24
                         |
                         |
                         |
                     usbB|.2
                     +---------+
  172.17.XX.0/24   .1| ROUTER  |.1   172.17.XX.0/24
 +-------------------+ HOST B  +--------------------+
      |.2        usbA|         |usbC        |.2
      |eth0          +---------+            |eth0
   +------+          eth0|.1             +------+
   |HOST A|              |               |HOST C|
   +------+              |               +------+
                         |
                         |
                         |
                         +
```

A. Introducir todas las rutas necesarias en el host B y listarlas

B. Lanzar fping para verificar que todos los equipos A, B y C responden

##### 6. Conectar todos los equipos del aula haciendo switching y routing.

Seguir este esquema


```
                                       172.16.0.0/24
           +------------------------------------------------------------------------------+
                        |.XX                                                     |.XX
                        |                                                        |
                        |br0(usbB,eth0)                                          |br0(usbB,eth0)
                    +---------+                                              +---------+
 172.17.XX.0/24   .1| ROUTER  |.1   172.17.XX.0/24        172.17.XX.0/24   .1| ROUTER  |.1   172.17.XX.0/24
+-------------------+ HOST B  +--------------------+     +-------------------+ HOST B  +--------------------+
     |.2        usbA|         |usbC        |.2                |.2        usbA|         |usbC        |.2
     |eth0          +---------+            |eth0              |eth0          +---------+            |eth0
  +------+                              +------+           +------+                              +------+
  |HOST A|                              |HOST C|           |HOST A|                              |HOST C|
  +------+                              +------+           +------+                              +------+

```

A. Introducir todas las rutas necesarias en el host B y listarlas

B. Lanzar fping para verificar que todos los equipos A, B y C responden
