EJERCICIO MIKROTIK 2.

OBJETIVO: CREAR DOS REDES WIFIS CON DISTINTOS TIPOS DE SEGURIDAD


 MIKROTIK
 +-------------------+
 |  1  2  3  4  wifi |
 +-------------------+
    |  |  |  |
    |  |  |  |
    |  |  |  +
    |  |  |  Conexión a un switch  con vlans
    |  |  +
    |  |  Interface de red placa base
    |  +
    |  Interface USB
    |  Ip 192.168.88.1XX/24
    +
    Punto de red del aula
    para salida a internet

## Pasos para conseguir tener 2 redes wifis separadas con distintos
niveles de seguridad.

Recordamos que:
- Solo disponemos de una interface wifi que trabaja en la banda de 2,4GHz.
- Este modelo de mikrotik dispone de 4 interfaces ethernet y 1 interface wifi
- En el PC dispondremos de una interface USB que es la que usaremos para
configurar el router por el puerto 2

Para conseguir dos redes wifi a partir de una sola interface hay que crear
sub-interfaces o interfaces virtuales. Cada interface virtual ha de estar 
asociada a un id de vlan (número de 1 a 2048)

Si queremos comunicarnos en la misma red entre un dispositivo wifi y un dispositivo
cableado hay que crear bridges entre la interface wifi y la interface ethernet.

### Eliminar configuración inicial
La configuración inicial que viene con la mikrotik podemos consultarla haciendo 
/export y obtenmos:

```
/interface bridge
add admin-mac=6C:3B:6B:30:B8:4F auto-mac=no comment=defconf name=bridge
/interface wireless
set [ find default-name=wlan1 ] band=2ghz-b/g/n channel-width=20/40mhz-Ce \
    disabled=no distance=indoors frequency=auto mode=ap-bridge ssid=\
    MikroTik-30B852 wireless-protocol=802.11
/interface ethernet
set [ find default-name=ether2 ] name=ether2-master
set [ find default-name=ether3 ] master-port=ether2-master
set [ find default-name=ether4 ] master-port=ether2-master
/ip neighbor discovery
set ether1 discover=no
set bridge comment=defconf
/ip pool
add name=default-dhcp ranges=192.168.88.10-192.168.88.254
/ip dhcp-server
add address-pool=default-dhcp disabled=no interface=bridge name=defconf
/interface bridge port
add bridge=bridge comment=defconf interface=ether2-master
add bridge=bridge comment=defconf interface=wlan1
/ip address
add address=192.168.88.1/24 comment=defconf interface=bridge network=\
    192.168.88.0
/ip dhcp-client
add comment=defconf dhcp-options=hostname,clientid disabled=no interface=ether1
/ip dhcp-server network
add address=192.168.88.0/24 comment=defconf gateway=192.168.88.1
/ip dns
set allow-remote-requests=yes
/ip dns static
add address=192.168.88.1 name=router
/ip firewall filter
add chain=input comment="defconf: accept ICMP" protocol=icmp
add chain=input comment="defconf: accept established,related" connection-state=\
    established,related
add action=drop chain=input comment="defconf: drop all from WAN" in-interface=\
    ether1
add action=fasttrack-connection chain=forward comment="defconf: fasttrack" \
    connection-state=established,related
add chain=forward comment="defconf: accept established,related" \
    connection-state=established,related
add action=drop chain=forward comment="defconf: drop invalid" connection-state=\
    invalid
add action=drop chain=forward comment="defconf:  drop all from WAN not DSTNATed" \
    connection-nat-state=!dstnat connection-state=new in-interface=ether1
/system routerboard settings
set boot-device=flash-boot cpu-frequency=650MHz protected-routerboot=disabled
/tool mac-server
set [ find default=yes ] disabled=yes
add interface=bridge
/tool mac-server mac-winbox
set [ find default=yes ] disabled=yes

```

A continuación modificamos todas esas partes de la configuración que no nos
interesan para dejar la mikrotik pelada, sin configuraciones iniciales
que interfieran con nuestra nueva programación del router:

```
# Cambiamos dirección IP para que la gestione directamente la interface 2
ip address set 0 interface=ether2-master

# Eliminamos el bridge

/interface bridge port remove 1
/interface bridge port remove 0
/interface bridge remove 0  

# Eliminamos que un puerto sea master de otro

/interface ethernet set [ find default-name=ether3 ] master-port=none
/interface ethernet set [ find default-name=ether4 ] master-port=none

# Cambiamos nombres a los puertos

/interface ethernet set [ find default-name=ether1 ] name=eth1
/interface ethernet set [ find default-name=ether2 ] name=eth2
/interface ethernet set [ find default-name=ether3 ] name=eth3
/interface ethernet set [ find default-name=ether4 ] name=eth4

# Deshabilitamos la wifi

/interface wireless set [ find default-name=wlan1 ] disabled=yes

# Eliminamos servidor y cliente dhcp

/ip pool remove 0
/ip dhcp-server network remove 0
/ip dhcp-server remove 0
/ip dhcp-client remove 0
/ip dns static remove 0
/ip dns set allow-remote-requests=no

# Eliminamos regla de nat masquerade
/ip firewall nat remove 0

```

Ahora le cambiamos el nombre y guardamos la configuración inicial por
si la necesitamos restaurar en cualquier momento
```
/system identity set name=mkt20
/system backup save name="20170323_zeroconf"
```

###[ejercicio1] Explica el procedimiento para resetear el router y restaurar
este backup, verifica con /export que no queda ninguna configuración inicial 
que pueda molestarnos para empezar a programar el router

	/system backup save name="20170317_zeroconf"
	/system reset --> user=admin/pass=none --> para resetear el router
	/system backup load name=20170317_zeroconf.backup	--> /file print para ver los backups

### Crear interfaces y bridges

Primero hay que pensar en capa 2, vamos a crear un bridge por cada red wifi
que tenga un punto de red cableado asociado a una vlan

Trabajaremos con dos vlans:

*120: red pública
*220: red privada

Hay que crear unas interfaces virtuales wifi adicional y cambiar los ssids
para reconocerlas cuando escaneemos las wifis

```
/interface wireless set 0 ssid="free120"
/interface wireless add ssid="private220" master-interface=wlan1
```



###[ejercicio2] Explica por qué aparecen las interfaces wifi como disabled
al hacer /interface print. Habilita y deshabilita estar interfaces. Cambiales
el nombre a wFree wPrivate y dejalas deshabilitadas

	/interface enable wlan1
	/interface enable wlan2
	/interface enable wlan3
	/interface enable wlan4
	
	/interface set name=wFree wlan2
	/interface set name=wPrivate wlan1
	
## Crear interfaces virtuales y hacer bridges

```
####AÑADIMOS unas interficies  VLAN con sus nombres e interficies
/interface vlan add name eth4-vlan120 vlan-id=120 interface=eth4
/interface vlan add name eth4-vlan220 vlan-id=220 interface=eth4   
	
####AÑADIMOS LOS BRIDGES							   
/interface bridge add name=br-vlan120
/interface bridge add name=br-vlan220

####AÑADIMOS LOS PUERTOS DE LOS BRIDGES
/interface bridge port add interface=eth4-vlan107 bridge=br-vlan120
/interface bridge port add interface=eth3 bridge=br-vlan120
/interface bridge port add interface=wFree  bridge=br-vlan120      


/interface bridge port add interface=eth4-vlan220 bridge=br-vlan220
/interface bridge port add interface=wPrivate   bridge=br-vlan220

/interface print
```

 
```
### [ejercicio3] Comenta cada una de estas líneas de la configuración
anterior para que quede bien documentado
```
		Flags: D - dynamic, X - disabled, R - running, S - slave 
	#     NAME                                TYPE       ACTUAL-MTU L2MTU  MAX-L2MTU MAC-ADDRESS      
	0     eth1                                ether            1500  1598       2028 E4:8D:8C:9A:9D:F0
	1  R  eth2                                ether            1500  1598       2028 E4:8D:8C:9A:9D:F1
	2   S eth3                                ether            1500  1598       2028 E4:8D:8C:9A:9D:F2
	3     eth4                                ether            1500  1598       2028 E4:8D:8C:9A:9D:F3
	4   S wFree                               wlan             1500  1600       2290 E6:8D:8C:9A:9D:F4
	5   S wPrivate                            wlan             1500  1600       2290 E4:8D:8C:9A:9D:F4
	6  R  br-vlan120                          bridge           1500  1598            E4:8D:8C:9A:9D:F2
	7  R  br-vlan220                          bridge           1500  1594            E4:8D:8C:9A:9D:F3
	8     eth4-vlan120                        vlan             1500  1594            E4:8D:8C:9A:9D:F3
	9   S eth4-vlan220                        vlan             1500  1594            E4:8D:8C:9A:9D:F3
		
```
### [ejercicio4] Pon una ip 172.17.120.1/24 a br-vlan120
y 172.17.220.1/24 a br-vlan220 y verifica desde la interface de la placa 
base de tu ordenador que hay conectividad (puedes hacer ping) configurando
unas ips cableadas. Cuando lo hayas conseguido haz un backup de la configuración
```
	ip address add interface=br-vlan120 address=172.17.120.1 netmask=255.255.255.0
	ip address add interface=br-vlan220 address=172.17.220.1 netmask=255.255.255.0
	ip address print
		Flags: X - disabled, I - invalid, D - dynamic 
		#   ADDRESS            NETWORK         INTERFACE                                                                                                            
		0   ;;; defconf
			192.168.88.1/24    192.168.88.0    eth2                                                                                                                 
		1   172.17.120.1/24    172.17.120.0    br-vlan120                                                                                                           
		2   172.17.220.1/24    172.17.220.0    br-vlan220  
		
	hace ping a la 172.17.120.1 conectar cable al puerto 3
	per no a la 172.17.220.1
		
```	
### [ejercicio5] Crea una servidor dhcp para cada una de las redes en los rangos
.101 a .250 de las respectivas redes. Asignar ip manual a la eth1. 
Crear reglas de nat para salir a internet. 
```
	ip pool add ranges=172.17.120.100-172.17.120.200 name=range_public
	ip pool add ranges=172.17.220.100-172.17.220.200 name=range_private 
	
	ip dhcp-server add interface=br-vlan120  address-pool=range_public 
	ip dhcp-server add interface=br-vlan220  address-pool=range_private

	ip dhcp-server set 0 disabled=no
	ip dhcp-server set 1 disabled=no

	[admin@mkt20] > ip dhcp-server print   
	Flags: X - disabled, I - invalid 
	#   NAME                               INTERFACE                               RELAY           ADDRESS-POOL                               LEASE-TIME ADD-ARP
	0   dhcp2                              br-vlan120                                              range_public                               10m       
	1   dhcp3                              br-vlan220                                              range_private                              10m       

	ip dhcp-server network add gateway=172.17.120.1 dns-server=8.8.8.8 netmask=24 domain=publico.com address=172.17.120.0/24
	ip dhcp-server network add gateway=172.17.220.1 dns-server=8.8.8.8 netmask=24 domain=privado.com address=172.17.220.0/24

```	
### [ejercicio6] Activar redes wifi y dar seguridad wpa2
```	
	interface wireless set wFree   ssid=mktfree20
	interface wireless set wPrivate   ssid=mktprivate20
	interface wireless security-profiles add wpa2-pre-shared-key="perico"
	interface wireless security-profiles set 1 authentication-types=wpa2-psk
		/interface wireless export                                               
		# mar/30/2017 09:28:49 by RouterOS 6.33.5
		# software id = A5MX-LWZC
		#
		/interface wireless
		set [ find default-name=wlan1 ] band=2ghz-b/g/n channel-width=20/40mhz-Ce disabled=no distance=indoors frequency=auto mode=\
			ap-bridge name=wPrivate ssid=mktprivate20 wireless-protocol=802.11
		add disabled=no mac-address=E6:8D:8C:9A:9D:F4 master-interface=wPrivate name=wFree ssid=mktfree20
		/interface wireless security-profiles
		set [ find default=yes ] supplicant-identity=MikroTik
		add authentication-types=wpa2-psk name=perico wpa2-pre-shared-key=perico
	
	interface wireless enable wFree 
	interface wireless enable wPrivate

```
### [ejercicio6b] Opcional (montar portal cautivo)
```
	
```
### [ejercicio7] Firewall para evitar comunicaciones entre las redes Free y Private. Limitar
puertos en Free.
```
		ip firewall nat export 
	# jan/02/1970 00:25:12 by RouterOS 6.33.5
	# software id = A5MX-LWZC
	#
	ip firewall nat add action=masquerade chain=srcnat out-interface=eth1 
	ip firewall nat print                                                 
		Flags: X - disabled, I - invalid, D - dynamic 
		0    chain=srcnat action=masquerade out-interface=eth1 log=no log-prefix="" 
	ip firewall nat export                                                
		# jan/02/1970 00:26:13 by RouterOS 6.33.5
		# software id = A5MX-LWZC
		#
		/ip firewall nat
		add action=masquerade chain=srcnat out-interface=eth1
	
	
	ip route add distance=1 gateway=192.168.0.1
	
	ip route export                            
	# jan/02/1970 00:32:43 by RouterOS 6.33.5
	# software id = A5MX-LWZC
	#
	/ip route
	add distance=1 gateway=192.168.0.1
	
	ip address add address=192.168.3.220/16 interface=eth1 network=192.168.0.0
	ip address export                                                         
	# jan/02/1970 00:34:08 by RouterOS 6.33.5
	# software id = A5MX-LWZC
	#
	/ip address
	add address=192.168.88.1/24 comment=defconf interface=eth2 network=192.168.88.0
	add address=172.17.120.1/24 interface=br-vlan120 network=172.17.120.0
	add address=172.17.220.1/24 interface=br-vlan220 network=172.17.220.0
	add address=192.168.3.220/16 interface=eth1 network=192.168.0.0
	
	####conecto el cable ethernet del pc al mikrotik me conecto a la wifi y tengo acceso a internet
```
### [ejercicio8] Conexión a switch cisco con vlans
```


```
### [ejercicio8b] (Opcional) Montar punto de acceso adicional con un AP de otro fabricante
```


```
### [ejercicio9] QoS
```


```
### [ejercicio9] Balanceo de salida a internet
```


```
### [ejercicio10] Red de servidores con vlan de servidores y redirección de puertos
```


```
### [ejercicio11] Firewall avanzado
```
