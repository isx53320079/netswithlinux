﻿﻿# Exercicis de Subxarxes

##### Instruccions generals:
1. Alerta, feu tots els càlculs sense calculadora!
2. Si necessiteu fer càlculs en un full apart, si us plau, indiqueu-m'ho en cada exercici i entregueu-me aquest full.
3. En total hi ha 5 exercicis (reviseu totes les pàgines)

##### 1. Donades les següents adreces IP, completa la següent taula (indica les adreces de xarxa i de broadcast tant en format decimal com en binari).


IP | Adreça de Xarxa | Adreça de Broadcast Extern | Rang IPs per a Dispositius 
---|---|---|---
172.16.4.23/25 |172.16.4.0|172.16.4.127|172.16.4.1 al 172.16.4.126
174.187.55.6/23 |174.187.54.0|174.187.55.255|174.187.54.1 al 174.187.54.254
10.0.25.253/18 |10.0.0.0|10.0.63.255|10.0.0.1 al 10.0.0.254
209.165.201.30/27 |209.165.201.0|209.165.201.31|209.165.201.1 al 209.165.201.31 


##### 2. Donada la xarxa 172.28.0.0/16, calcula la màscara de xarxa que necessites per poder fer 80 subxarxes. Tingues en compte que cada subxarxa ha de tenir capacitat per a 400 dispositius. Indica la màscara de xarxa calculada tant en format decimal com en binari.

**Amb /23 tinc 510 hosts**. Justificació:
* 32 - 23 = 9 bits per host
* 2^9 = 512 combinacions posibles
* 512 - 2 adreçes reservades = 510 hosts

De /16 a /23 hi han 7 bits per fer combinacions de subxarxes. 2^7 = 128 combinacions = 128 subxarxes posibles

La màscara CIDR /23 en format binari és: 

    11111111.11111111.11111110.00000000
    
i en format decimal es: 

    255.255.254.0 


##### 3. Donada la xarxa 10.192.0.0
a) Calcula la màscara de xarxa que necessites per poder adreçar 1500 dispositius. Indica-la tant en format decimal com en binari.
 
 * 32 - 21 = 11
 * 2^11 = 2048
 * 2048 - 2 = 2046hosts
 
    255.255.248.0 = /21
    
    11111111.11111111.11111000.00000000


b) Si fixem l'adreça de la xarxa mare a la 10.192.0.0/10, quantes subxarxes de 1500 dispositius podrem fer?

 * del /10 al /21 sn 11 bits, 2^11 = 2048 subredes posibles


c) Si ara considerem que l'adreça de xarxa mare és la 10.192.0.0 amb la màscara de xarxa calculada a l'apartat (a), calcula la nova màscara de xarxa que permeti fer 3 subxarxes de 500 dispositius cadascuna. Indica-la tant en format decimal com en binari.
 * con 2^1 tengo 2 subredes(/22)
 * con 2^2 tengo 4 subredes(/23) enteonces como necesito 3 me quedo con esta
 * 32 - 23 de la mascara = 9
 * 2^9 = 512 hosts menos 2 de reservados = 510 hosts encada subred
 
la màscara CIDR /23 en format binari és: 

    11111111.11111111.11111110.00000000
    
i en format decimal es: 

    255.255.254.0 
    
##### 4. Donada la xarxa 10.128.0.0 i sabent que s'ha d'adreçar un total de 3250 dispositius

a) Calcula la màscara de xarxa per crear una adreça de xarxa mare que permeti adreçar tots els dispositius indicats. Indica-la tant en format decimal com en binari.
* 32 - 20 = 12
* 2^12 - 2 = 4094 hosts/dispositius

    255.255.240.0 = /20
    
    11111111.11111111.11110000.00000000


b) Calcula la nova màscara de xarxa que permeti fer 5 subxarxes amb 650 dispositius cadascuna d'elles. Indica-la tant en format decimal com en binari.

* con 2^1 tengo 2 subredes 
* con 2^2 tengo 4 subredes
* con 2^3 tengo 8 subredes(/23) como necesito 5 subredes me quedo con esta



c) Comprova que, realment, cada subxarxa pot adreçar els 650 dispositius demanats. En cas que no sigui així, quina és la capacitat màxima de dispositius de cadascuna d'aquestes xarxes? Indica la fórmula que fas anar per calcular aquesta dada.

* 32 - 23 = 9
* 2^9 = 512
* 512 - 2 = 510 (8 subredes de 510 dispositivos)

d) A partir de la xarxa mare que has obtingut amb la màscara de xarxa calculada a l'apartat (a), podem fer dues subxarxes amb 1625 dispositius cadascuna d'elles? Indica els càlculs que necessites fer per raonar la teva resposta.

* con 2^1 = 2 subredes (/21)
* 32 - 21 = 11 
* 2^11 = 2048
* 2048 - 2 = 2046 dispositius encada subxarxa

##### 5. Donada l'adreça de xarxa mare (AX mare) 172.16.0.0/12, s'han de calcular 6 subxarxes.

a) Quants bits necessites per ampliar la Màscara de Xarxa (MX) per tal de poder fer aquestes 6 subxarxes?

* 2^3 = 8 (/15)
* 3 bits 

b) Dóna la nova MX, tant en format decimal com en binari.

a màscara CIDR /15 en format binari és: 

    11111111.11111110.00000000.00000000
    
i en format decimal es: 

    255.254.0.0

c) Per cada subxarxa nova que has de crear, indica
i. L'adreça de xarxa, en format decimal i en binari
ii. L'adreça de broadcast extern, en format decimal i en binari
iii. El rang d'IPs per a dispositius

* 32 - 15 = 17 
* 2^17 = 131070
* 131070 - 2 = 131068 dispositius per a cada subred

Nom| Adreça de Xarxa | Adreça de Broadcast Extern | Rang IPs per a Dispositius
---|---|---|---
Xarxa 1 | 172.16.0.1 - 172.17.255.254 | 172.17.255.255 | 131068
Xarxa 2 | 172.18.0.1 - 172.19.255.254 | 172.19.255.255 | 131068
Xarxa 3 | 172.20.0.1 - 172.21.255.254 | 172.21.255.255 | 131068
Xarxa 4 | 172.22.0.1 - 172.23.255.254 | 172.23.255.255 | 131068
Xarxa 5 | 172.24.0.1 - 172.25.255.254 | 172.25.255.255 | 131068
Xarxa 6 | 172.26.0.1 - 172.27.255.254 | 172.27.255.255 | 131068


d) Tenint en compte el número de bits que has indicat en l'apartat a), quantes subxarxes podríem fer, realment? Dóna'n la fórmula que s'utilitza per calcular aquesta dada.

* 2^3 = 8 subredes 

e) Segons la MX que has calculat als apartats a) i b), quants dispositius pot tenir cada subxarxa? Dóna'n la fórmula que s'utilitza per calcular aquesta dada.

* 32 - 15 = 17
* 2^17 = 131070
* 32768 - 2 = 131068 dispositius per a cada subred


##### 6. Donada l'adreça de xarxa mare (AX mare) 192.168.1.0/24, s'han de calcular 4 subxarxes.

a) Quants bits necessites per ampliar la Màscara de Xarxa (MX) per tal de poder fer aquestes 6 subxarxes?

* 2 bits

b) Dóna la nova MX, tant en format decimal com en binari.

* 255.255.255.192 (/26)
* 11111111.11111111.11111111.11000000

c) Per cada subxarxa nova que has de crear, indica
i. L'adreça de xarxa, en format decimal i en binari
ii. L'adreça de broadcast extern, en format decimal i en binari
iii. El rang d'IPs per a dispositius

Nom| Adreça de Xarxa | Adreça de Broadcast Extern | Rang IPs per a Dispositius
---|---|---|---
Xarxa 1| 192.168.1.1 - 192.168.1.62 | 192.168.1.63 | 62
Xarxa 2| 192.168.1.65 - 192.168.1.126 | 192.168.1.127 | 62
Xarxa 3| 192.168.1.129 - 192.168.1.190 | 192.168.1.191 | 62
Xarxa 4| 192.168.1.193 - 192.168.1.254 | 192.168.1.255 | 62


d) Tenint en compte el número de bits que has indicat en l'apartat a), podríem fer més de 4 subxarxes? Dóna'n la fórmula que has utilitzat per respondre a la pregunta.

* 2^2 = 4 subredes 
* NO, somes 4 subxarxes

e) Segons la MX que has calculat als apartats a) i b), quants dispositius pot tenir cada subxarxa? Dóna'n la fórmula que s'utilitza per calcular aquesta dada.

* 32 - 26 = 6
* 2^6 = 64
* 64 -2 = 62 hosts
