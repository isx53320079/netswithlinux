#EJERCICIO MIKROTIK 1.

##OBJETIVO: FAMILIARIZARSE CON LAS OPERACIONES BÁSICAS EN UN ROUTER MIKROTIK

###1. Entrar al router con telnet, ssh y winbox

	telnet 192.168.88.1
	
	ssh admin@192.168.88.1
	

###2. Como resetear el router

	Mientras conectas el router a la corriente al mismo tiempo mantienes pulsado el boton de RES hasta 
	que el led de ACT empieza a parpadear

###3. Cambiar nombre del dispositivo, password 

	[admin@DaniCano] /system identity> edit name --> nos saldra para cambiar el nombre

	
###4. Esquema de configuración de puertos inicial

	> interface ethernet print 
	
	Flags: X - disabled, R - running, S - slave 
	#    NAME        MTU MAC-ADDRESS       ARP        MASTER-PORT      SWITCH
	0    ether1     1500 E4:8D:8C:9A:9D:F0 enabled    none             switch1
	1 RS ether2...  1500 E4:8D:8C:9A:9D:F1 enabled    none             switch1
	2  S ether3     1500 E4:8D:8C:9A:9D:F2 enabled    ether2-master    switch1
	3  S ether4     1500 E4:8D:8C:9A:9D:F3 enabled    ether2-master    switch1

###5. Cambiar nombres de puertos y deshacer configuraciones iniciales

	

###6. Backup y restauración de configuraciones
	
	/system backup save name="20170317_zeroconf"
	system reboot 
	system backup load name=20170317_zeroconf.backup
	
