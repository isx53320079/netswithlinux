Examen de routing con mikrotik

[admin@mkt20] > interface print
Flags: D - dynamic, X - disabled, R - running, S - slave 
 #     NAME                                TYPE       ACTUAL-MTU L2MTU
 0     eth1                                ether            1500  1598
 1  R  eth2                                ether            1500  1598
 2     eth3                                ether            1500  1598
 3     eth4                                ether            1500  1598
 4  X  wlan1                               wlan             1500  1600



## configuracion de ips:

[admin@mkt20] > ip address add address=192.168.200.1/24 interface=eth4--> entramos por este
[admin@mkt20] > ip address add address=192.168.3.100/24 interface=eth3--> salimos por este

[admin@mkt20] > ip pool add name=internal ranges=192.168.200.150-192.168.200.200

[admin@mkt20]ip dhcp-server network add dns-server=8.8.8.8 gateway=192.168.200.1 netmask=24 domain=meloinvento.com address=192.168.200.0/24 
[admin@mkt20] > ip dhcp-server add address-pool=internal interface=eth4 disabled=no          
     
[admin@mkt20] > ip dhcp-server print                                            Flags: X - disabled, I - invalid                              
 #   NAME     INTERFACE     RELAY           ADDRESS-POOL     LEASE-TIME ADD-ARP
 0   dhcp1    eth4                          internal         10m       

[admin@mkt20]ip address print
Flags: X - disabled, I - invalid, D - dynamic 
 #   ADDRESS            NETWORK         INTERFACE                              
 0   ;;; defconf
     192.168.88.1/24    192.168.88.0    eth2                                   
 1   192.168.200.1/24   192.168.200.0   eth4                                   
 2   192.168.3.100/24   192.168.3.0     eth3    

[admin@mkt20]ip firewall nat add action=masquerade chain=srcnat out-interface=eth3 --> para hacer ping al 8.8.8.8
[admin@mkt20] > ping 8.8.8.8
  SEQ HOST                                     SIZE TTL TIME  STATUS           
    0                                                         no route to host 
    1                                                         no route to host 
    2                                                         no route to host 
    sent=3 received=0 packet-loss=100% 

[admin@mkt20] > ip route print--> nos falta el la ruta estatica
Flags: X - disabled, A - active, D - dynamic, 
C - connect, S - static, r - rip, b - bgp, o - ospf, m - mme, 
B - blackhole, U - unreachable, P - prohibit 
 #      DST-ADDRESS        PREF-SRC        GATEWAY            DISTANCE
 0  DC  192.168.3.0/24     192.168.3.100   eth3                    255
 1 ADC  192.168.88.0/24    192.168.88.1    eth2                      0
 2  DC  192.168.200.0/24   192.168.200.1   eth4                    255

[admin@mkt20] ip route add dst-address=0.0.0.0/0 gateway=192.168.0.1 --> poner ruta estatica






##cortar trafico entre dos interfaces
[admin@mkt20] > ip firewall filter add chain=forward in-interface=eth4 out-interface=eth2 action=drop  place-before=0   
[admin@mkt20] > ip firewall filter add chain=forward in-interface=eth2 out-interface=eth4 action=drop  place-before=0  
[admin@mkt20] > ip firewall filter print                                        Flags: X - disabled, I - invalid, D - dynamic 
 0    chain=forward action=drop in-interface=eth4 out-interface=eth2 log=no 
      log-prefix="" 

 1    chain=forward action=drop in-interface=eth2 out-interface=eth4 log=no 
      log-prefix="" 

 2    chain=forward action=accept in-interface=eth4 out-interface=eth2 log=no 
      log-prefix="" 

 3  D ;;; special dummy rule to show fasttrack counters
      chain=forward 
      
[admin@mkt20] > ip firewall filter add chain=forward dst-address=8.8.4.4 action=drop  place-before=0  

[admin@mkt20] > ip firewall filter  add dst-port=!443 protocol=tcp chain=forward out-interface=eth3 action=drop place-before=0





[admin@mkt20] > interface vlan add vlan-id=100 interface=eth4 name=virtual100
[admin@mkt20] > interface print 
Flags: D - dynamic, X - disabled, R - running, S - slave 
 #     NAME                                TYPE       ACTUAL-MTU L2MTU  MAX-L2MTU
 0     eth1                                ether            1500  1598       2028
 1  R  eth2                                ether            1500  1598       2028
 2     eth3                                ether            1500  1598       2028
 3     eth4                                ether            1500  1598       2028
 4  X  wlan1                               wlan             1500  1600       2290
 5     virtual100                          vlan             1500  1594

[admin@mkt20] ip link add link enp2s0 name ethv100 type vlan id 100

 <<root>> ip link add link enp2s0 name ethv100 type vlan id 100
			ip link set eth100 up
