MIKTORIK

1. Permitir conexiones activas como primera regla de filter
/ip firewall filter 
add action=accept chain=forward connection-state=established

2. Impedir tráfico entre red 192.168.3.0/24 y 192.168.4.0/24
ip firewall filter  add  chain=forward src-address=192.168.3.0/24 dst-address= 192.168.4.0/24 action=drop 
ip firewall filter  add  chain=forward src-address=192.168.4.0/24 dst-address= 192.168.3.0/24 action=drop 

3. Marcar conexiones que salen desde 192.168.4.3

ip firewall mangle 
add chain=prerouting action=mark-connection new-mark-connection=host3 src-address=192.168.4.3 

4. Priorizar tráfico para equipos que tienen como origen 192.168.4.3
/ QUEUETREE
/ip firewall mangle
add chain=prerouting action=mark-packet mark-connection=host3 in-interface=eth0 
new-packet-mark=from_internet_to_host_3
/queue tree
add 
    priority=20 name=queue1 packet-mark=from_internet_to_host_3 parent=eth1 queue=\
    pcq-download-default

5. Marcar conexiones y routing-mark para equipos 192.168.4.128/25
https://gitlab.com/beto/netswithlinux/raw/master/scripts/mkt_trunk_router.py
/ip firewall mangle 
add chain=prerouting src-address=192.168.4.128/25 action=mark-packet new-packet-mark=vienede4128
add action=mark-connection chain=prerouting connection-state=new \
    new-connection-mark=marca src-address=192.168.4.128/25
    
6. Hacer que el gateway por defecto sea 192.168.1.1 excepto para 192.168.4.128/25
que será 192.168.1.1

https://wiki.mikrotik.com/wiki/Manual:IP/Firewall/Filter

