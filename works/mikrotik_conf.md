# may/11/2017 09:44:30 by RouterOS 6.33.5
# software id = A5MX-LWZC
#
/ip pool
add name=pool2 ranges=10.17.2.100-10.17.2.199
add name=pool3 ranges=10.17.3.100-10.17.3.199
add name=pool4 ranges=10.17.4.100-10.17.4.199
add name=pool5 ranges=10.17.5.100-10.17.5.199
add name=pool6 ranges=10.17.6.100-10.17.6.199
/ip address
add address=192.168.88.1/24 comment=defconf interface=eth2 network=192.168.88.0
add address=192.168.3.207/16 interface=eth3 network=192.168.0.0
add address=10.107.2.1/24 interface=vnet2 network=10.107.2.0
add address=10.107.3.1/24 interface=vnet3 network=10.107.3.0
add address=10.107.4.1/24 interface=vnet4 network=10.107.4.0
add address=10.107.5.1/24 interface=vnet5 network=10.107.5.0
add address=10.107.6.1/24 interface=vnet6 network=10.107.6.0
add address=10.17.2.1/24 interface=vnet2 network=10.17.2.0
add address=10.17.3.1/24 interface=vnet3 network=10.17.3.0
add address=10.17.4.1/24 interface=vnet4 network=10.17.4.0
add address=10.17.5.1/24 interface=vnet5 network=10.17.5.0
add address=10.17.6.1/24 interface=vnet6 network=10.17.6.0
/ip dhcp-server
add address-pool=pool2 disabled=no interface=vnet2 name=dhcp1
add address-pool=pool3 disabled=no interface=vnet3 name=dhcp2
add address-pool=pool4 disabled=no interface=vnet4 name=dhcp3
add address-pool=pool5 disabled=no interface=vnet5 name=dhcp4
add address-pool=pool6 disabled=no interface=vnet6 name=dhcp5
/ip dhcp-server network
add address=10.17.2.0/24 dns-server=8.8.8.8 domain=meloinvento.com gateway=10.17.2.1 netmask=24
add address=10.17.3.0/24 dns-server=8.8.8.8 domain=meloinvento.com gateway=10.17.3.1 netmask=24
add address=10.17.4.0/24 dns-server=8.8.8.8 domain=meloinvento.com gateway=10.17.4.1 netmask=24
add address=10.17.5.0/24 dns-server=8.8.8.8 domain=meloinvento.com gateway=10.17.5.1 netmask=24
add address=10.17.6.0/24 dns-server=8.8.8.8 domain=meloinvento.com gateway=10.17.6.1 netmask=24
/ip firewall filter
add chain=input comment="defconf: accept ICMP" protocol=icmp
add chain=input comment="defconf: accept establieshed,related" connection-state=established,related
add action=drop chain=input comment="defconf: drop all from WAN" in-interface=eth1
add action=fasttrack-connection chain=forward comment="defconf: fasttrack" connection-state=established,related
add chain=forward comment="defconf: accept established,related" connection-state=established,related
add action=drop chain=forward comment="defconf: drop invalid" connection-state=invalid
add action=drop chain=forward comment="defconf:  drop all from WAN not DSTNATed" connection-nat-state=!dstnat connection-state=new in-interface=eth1
/ip firewall mangle
add action=mark-packet chain=prerouting in-interface=vnet2 new-packet-mark=fromvnet2
/ip firewall nat
add action=masquerade chain=srcnat out-interface=eth3
/ip route
add distance=1 gateway=192.168.0.1
add distance=1 dst-address=10.101.0.0/17 gateway=192.168.3.201
add distance=1 dst-address=10.102.0.0/17 gateway=192.168.3.202
add distance=1 dst-address=10.103.0.0/17 gateway=192.168.3.203
add distance=1 dst-address=10.104.0.0/17 gateway=192.168.3.204
add distance=1 dst-address=10.105.0.0/17 gateway=192.168.3.205
add distance=1 dst-address=10.106.0.0/17 gateway=192.168.3.206
add distance=1 dst-address=10.107.0.0/17 gateway=192.168.3.207
add distance=1 dst-address=10.108.0.0/17 gateway=192.168.3.208
add distance=1 dst-address=10.109.0.0/17 gateway=192.168.3.209
add distance=1 dst-address=10.110.0.0/17 gateway=192.168.3.210
add distance=1 dst-address=10.111.0.0/17 gateway=192.168.3.211
add distance=1 dst-address=10.112.0.0/17 gateway=192.168.3.212
add distance=1 dst-address=10.113.0.0/17 gateway=192.168.3.213
add distance=1 dst-address=10.114.0.0/17 gateway=192.168.3.214
add distance=1 dst-address=10.115.0.0/17 gateway=192.168.3.215
add distance=1 dst-address=10.116.0.0/17 gateway=192.168.3.216
add distance=1 dst-address=10.117.0.0/17 gateway=192.168.3.217
add distance=1 dst-address=10.118.0.0/17 gateway=192.168.3.218
add distance=1 dst-address=10.119.0.0/17 gateway=192.168.3.219
add distance=1 dst-address=10.120.0.0/17 gateway=192.168.3.220
add distance=1 dst-address=10.121.0.0/17 gateway=192.168.3.221
add distance=1 dst-address=10.122.0.0/17 gateway=192.168.3.222
add distance=1 dst-address=10.123.0.0/17 gateway=192.168.3.223
add distance=1 dst-address=10.124.0.0/17 gateway=192.168.3.224
add distance=1 dst-address=10.125.0.0/17 gateway=192.168.3.225
add distance=1 dst-address=10.126.0.0/17 gateway=192.168.3.226
add distance=1 dst-address=10.127.0.0/17 gateway=192.168.3.227
add distance=1 dst-address=10.128.0.0/17 gateway=192.168.3.228
add distance=1 dst-address=10.129.0.0/17 gateway=192.168.3.229
add distance=1 dst-address=10.130.0.0/17 gateway=192.168.3.230
add distance=1 dst-address=10.131.0.0/17 gateway=192.168.3.231
add distance=1 dst-address=10.132.0.0/17 gateway=192.168.3.232
add distance=1 dst-address=10.133.0.0/17 gateway=192.168.3.233
add distance=1 dst-address=10.134.0.0/17 gateway=192.168.3.234
add distance=1 dst-address=10.135.0.0/17 gateway=192.168.3.235
