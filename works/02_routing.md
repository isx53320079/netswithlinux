A partir del esquema de redes e ips del aula:

                                                    +------------ internet
                                                    |
                                                +-------+
                       +------------------------+ PROFE +---------------------+
                       |                        +-------+                     |
      2.1.1.0/24       |.254                                 2.1.2.0/24       |.254
      +-----------------------------------+                  +-----------------------------------+
            |.6        |.5        |.4                              |.3        |.2        |.1
            |          |          |                                |          |          |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
         |PC06 |    |PC05 |    |PC04 |                          |PC03 |    |PC02 |    |PC01 |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
                       |                                                      |
      2.2.1.0/24       |.254                                 2.2.2.0/24       |.254
      +-----------------------------------+                  +-----------------------------------+
            |.12       |.11       |.10                             |.9        |.8        |.7
            |          |          |                                |          |          |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
         |PC12 |    |PC11 |    |PC10 |                          |PC09 |    |PC08 |    |PC07 |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
                       |                                                      |
      2.3.1.0/24       |.254                                 2.3.2.0/24       |.254
      +-----------------------------------+                  +-----------------------------------+
            |.18       |.17       |.16                             |.15       |.14       |.13
            |          |          |                                |          |          |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
         |PC18 |    |PC17 |    |PC16 |                          |PC15 |    |PC14 |    |PC13 |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
                       |                                                      |
      2.4.1.0/24       |.254                                 2.4.2.0/24       |.254
      +-----------------------------------+                  +-----------------------------------+
            |.24       |.23       |.22                             |.21       |.20       |.19
            |          |          |                                |          |          |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
         |PC24 |    |PC23 |    |PC22 |                          |PC21 |    |PC20 |    |PC19 |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
                       |                                                      |
      2.5.1.0/24       |.254                                 2.5.2.0/24       |.254
      +-----------------------------------+                  +-----------------------------------+
            |.30       |.29       |.28                             |.27       |.26       |.25
            |          |          |                                |          |          |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
         |PC30 |    |PC29 |    |PC28 |                          |PC27 |    |PC26 |    |PC25 |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
         
###### 0. Identifica cual es tu PC, que ips/máscaras ha de llevar sobre qué interfaces

         --> pc20
         --> mascara: 255.255.255.0
         --> ip: 2.4.2.20
         
###### 1. Lista de órdenes para poder introducir manualmente las direcciones ip de tu puesto de trabajo con la orden ip y evitar que el NetworkManager o configuraciones previas interfieran en estas configuraciones
		
		 --> ifconfig (enp2s0)
		 --> ip a a 2.4.2.20/24 dev enp2s0, añadimos ip
		 --> systemctl stop NetworkManager
		
###### 2. Lista de órdenes para configurar el PC de tu fila como router con un comentario explicando lo que hace cada línea

###### 3. Explicación de cada línea de la salida del comando ip route show

	--> default via 192.168.0.5 dev enp2s0  proto static  metric 100 
	192.168.0.0/16 dev enp2s0  proto kernel  scope link  src 192.168.3.7  metric 100 

###### 4. Hacer un ping a un pc de tu misma fila y al PC19 y al PC24. 

**Capturar los paquetes con wireshark y guardar la captura. Analizarla 
observando el campo TTL del protocolo IP. Explicar los diferentes valores
de ese campo en función de la ip a la que se ha hecho el ping**

###### 5. Hacer un traceroute y un mtr al 8.8.8.8 Explicar a partir de que ip se corta y por qué. 

###### 6. Explica que es el enmascaramiento y cómo se aplicaría en el ordenador del profe para que de salida a internet
	
	-->la ip del profe "canviaria" para salir a internet
	-->enmascara todo el trafico que va hacia internet
