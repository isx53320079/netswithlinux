# WiFi 

Los dispositivos wifi habituales en un entorno profesional suelen ser
Access Points dedicados o integrados en routers. 

Internamente algunos de estos dispositivos funcionan con un kernel de 
linux, otros con un firmware propio. En la mayoría de dispositivos
encapsulan este software interno con una capa de servidor web para 
configurarse, o con una línea de comandos propia de cada fabricante. 

## Explicación del mercado de APs y instalaciones wifi.

La gama más económica, orientada al sector doméstico está dominada por
fabricantes como Dlink, Netgear, Conceptronic, 
Linksys(línea económica de cisco) o TP-Link.

Si lo que queremos es montar una red wifi con un nivel avanzado de 
control sobre los dispositivos y orientados al sector más profesional
nos encontramos con marcas como Ubiquti, Mikrotik o Edimax.

Si hay que hacer un despliegue con muchos puntos de acceso que requieren
de un control centralizado pasamos a necesitar APs gobernados por un
controlador centralizado con líneas de producto como Aironet de Cisco, 
Unifi de Ubiquiti, Ruckus o Aruba de HP.

### Firmware libre 

La historia de los firmwares libres se remonta a 2003 cuando se descubrió
que un firmware que llevaba un router wifi de Linksys, en concreto el modelo WRT54G
llevaba internamente un kernel de linux con software GPL encima. La licencia
GPL obliga a hacer públicas las modificaciones del código si lo has rehusado.

A partir de esta denuncia cisco se vió obligada a publicar el código. Partiendo
de ese código se pudieron crear firmwares libres con un linux y sus
herramientas asociadas como dd-wrt, tomato o openwrt. Openwrt se consolidó
al integrar un buen gestor de paquetes (opkg) y soporte para multitud
de routers de diferentes marcas. En 2016 se lanzó lede, un proyecto que 
es un fork de openwrt con la pretensión de abrir su comunidad de desarrolladores.




# ROUTERS MIKROTIK                   
## Introducción

Para realizar las prácticas de firewalls y políticas de routing y VPN utilizaremos routers de la marca Mikrotik. Estos routers internamente llevan un kernel de linux con un set de instrucciones que facilita la programación de las diferentes opciones. Disponen de una herramienta con un entorno gráfico que permite interactuar con el router de forma más intuitiva.

La familia de routers mikrotik se caracterizan por ofrecer un gran número de configuraciones y opciones a un precio muy asequible (desde 40 euros). El sistema operativo que se encuentra preinstalado en los routers (router OS), también puede comprarse para correr sobre un PC y es el mismo en todos sus modelos.

Algunos equipos también llevan una wifi integrada y otros permiten insertar hasta 3 tarjetas wifi mini-PCI. También disponen de un portal cautivo.

## Programar el router

Hay que acceder a estos routers tanto desde la línea de comandos como desde la interfaz gráfica y aprender las ventajas e inconvenientes de las dos formas de programar el router. La interfaz gráfica irá bien para probar y descubrir las opciones del router, observar el comportamiento en tiempo real, o activar o desactivar una regla. La interfaz de línea de comandos será útil para replicar las configuraciones en otros routers, lanzar muchas órdenes de golpe, realizar scripts...

Para acceder por línea de comandos se puede acceder por telnet o por ssh, las mikrotik disponen de un servidor telnet y un servidor ssh que atiende peticiones de clientes que quieran conectarse usando estos protocolos. Telnet no cifra las comunicaciones ni los passwords, con lo que no es un entorno aconsejado para acceder desde redes públicas o donde pueda haber riesgos de seguridad (por ejemplo las wifis), es un protocolo muy rápido de acceso. SSH es el protocolo estándar para comunicarnos por línea de comandos con cualquier dispositivo que requiera una comunicación segura, tiene otra ventaja añadida ya que permite la autentificación por certificados sin que pida un password, lo que facilita el trabajo para poder lanzar órdenes en equipos remotos sin necesidad de una intervención humana (con un script por ejemplo).

En el entorno windows se suele acceder por ssh o telnet con el programa putty (se descarga gratuito de internet), mientras que en cualquier distribución linux están disponibles los clientes con las órdenes “telnet” y “ssh”. Tener instalado un cliente de telnet en linux hay distribuciones que consideran que es inseguro, con lo que no viene por defecto, para instalarlo:

    # dnf -y install telnet

Para acceder por telnet sólo es necesario escribir la ip o el nombre dns del servidor donde quiero conectarme, y el equipo remoto ya se encarga de pedirme un usuario y un password. Telnet utiliza el puerto 23 TCP y no suele modificarse.

```
telnet nombre_del_servidor
```

ssh requiere que le pasemos como parámetros el nombre de usuario con el que queremos validarnos. Si no le pasamos el nombre de usuario asume que el nombre de usuario con el que te quieres validar es el mismo que tienes en la sesión actual de linux. La forma típica es:

    ssh usuario@servidor

Si queremos validarnos con un certificado podemos usar la opción -i fichero_con_clave_privada. Otra opción típica es modificar el puerto de ssh, en internet es común que intenten realizar ataques probando nombres de usuario y passwords típicos, ya que por desconocimiento o por descuídos, por ejemplo hay veces que podríamos dejar un usuario admin con password admin en una ip pública de internet. Para evitar este tipo de ataques hay una recomendación que siguen muchos administradores de red, se trata de modificar el puerto típico de ssh (puerto 22 TCP) en algún número de puerto no típico (por ejemplo 2222). Para acceder entonces a ese servidor ssh hay que especificar el puerto con la opción -p.

Tiene una ROM interna donde se almacenan ficheros, pensada sobre todo para poder hacer backups o almacenar scripts. Para poder acceder a estos ficheros se dispone de un servidor FTP y acceso a los ficheros por SSH (versión segura).

## Configuración inicial

```
The following default configuration has been installed on your router:


Wireless Configuration:
    security-key:    46ED02B4A4AF;
    mode:        ap-bridge;
    band:        2ghz-b/g/n;
    frequency:    2412;
    ht-chains:    two;
    ht-extension:    20/40mhz-ht-above;
WAN (gateway) Configuration:
    gateway:    ether1 (renamed with extension '-gateway');
    firewall:     enabled;
    NAT:        enabled;
    DHCP Client:    enabled;
LAN Configuration:
    LAN Port:    bridge-local;
    switch group:    ether2 (master), ether3, ether4, ether5
        (renamed with extensions '-master-local' and '-slave-local')
    LAN IP:        192.168.88.1;
    DHCP Server:    enabled;


You can click on "Show Script" to see the exact commands that are used to add and remove this default configuration.To remove this default configuration click on "Remove Configuration" or click on "OK" to continue.


NOTE: If you are connected using the above IP and you remove it, you will be disconnected.

```

En los routers Mikrotik suele haber una configuración de ips por defecto en la configuración inicial. Si disponemos de 5 puertos y una wifi encontramos que:

El puerto 1 es una interfaz ethernet que pide una ip por dhcp. Será el puerto por el que saldrá     el router a internet.    

El puerto 2 es una interfaz que forma parte de un bridge junto con la interfaz de wifi. Es decir la wifi y el puerto 2 están en la misma subred

Los puertos 3, 4 y 5 son interfícies dependientes de la 2, es decir, copian la configuración     del puerto 2.

En resumen los puertos 2, 3, 4, 5 y la wifi funcionan como un switch, y se gobiernan desde una misma ip que está asociada al bridge, el bridge tiene asociada una     interfície virtual que forma parte del switch y permite que al conectarnos por cualquiera de estos puertos podamos acceder al router a través de la ip 192.168.88.1/24

El router tiene activo un servidor dhcp que da ips en la red 192.168.88.0/24, con lo que al conectarnos     a esta subred podemos disponer de una ip automática. Aunque es recomendable ponerse una ip fija     

Para entrar por primera vez en el router podemos conectar el puerto 2 del router a la interfaz usb-ethernet del pc del aula. Linux reconoce la interfaz usb-ethernet como eth1. Podemos activar la interfície pedir una ip por dhcp y comprobar que podemos hacer ping.

```
# systemctl stop NetworkManager
# service network stop
# ip link set eth0 up   =>  activa la interrficie eth0
# dhclient -r => “release” suelta la ip que tuviera asignada por dhcp
# dhclient eth0 => pide una ip por dhcp para la interfícieeth0
# ip link set eth1 up => levantgo la interficie eth1
# ip a a 192.168.88.2XX/24 dev eth1 --> asigna una ip al la interfaz usb
# ping 192.168.88.1 => PING AL ROUTER
```

Una vez hemos verficicado que tenemos conectividad haciendo un ping al router ya podemos intentar entrar por línea de comandos la primera vez.

```
# telnet 192.168.88.1
MikroTik v6.6
Login: admin
Password:

MMM MMM KKK TTTTTTTTTTT KKK
MMMM MMMM KKK TTTTTTTTTTT KKK
MMM MMMM MMM III KKK KKK RRRRRR OOOOOO TTT III KKK KKK
MMM MM MMM III KKKKK RRR RRR OOO OOO TTT III KKKKK
MMM MMM III KKK KKK RRRRRR OOO OOO TTT III KKK KKK
MMM MMM III KKK KKK RRR RRR OOOOOO TTT III KKK KKK

MikroTik RouterOS 6.6 (c) 1999-2013 http://www.mikrotik.com/

[?] Gives the list of available commands
command [?] Gives help on the command and list of arguments

[Tab] Completes the command/word. If the input is ambigous,
a second [Tab] gives possible options

/ Move up to base level
.. Move up one level
/command Use command at the base level

[admin@routerA] >

```

Vale la pena deternos un momento a mirar la información de bienvenida que nos da la mikrotik.
La versión del sistema operativo     es RouterOS 6.6 --> es importante conocer la versión, y     podríamos conectarnos a internet y mirar si en la página web del     fabricante hay una versión más actualizada a la que valga la pena     cambiar.
    
Hay una serie de teclas que pueden     de gran ayuda, en especial aprender a utilizar
    
“?” nos muestra las opciones         de los comandos
        
“Tab” nos facilita la         escritura de reglas al ayudarnos a autocompletar
        
Los comandos están estructurados         en menús, en el prompt nos indican en que rama del menú estamos.         Con “/” volvemos al menú base, y con “..” al menú         anterior
    

Escribimos la orden “password” y ponemos el nuevo password como telemática. Con la orden “quit” salimos del router y se cierra el cliente de telnet.

## Primeros pasos con winbox
Para conectarnos por winbox utilizamos wine (emulador de windows). 
Descargar winbox:

```
# wget http://download2.mikrotik.com/routeros/winbox/3.0beta3/winbox.exe
# wine winbox.exe
```
