#Ejemplo-Guia

	Router> enable
	Router# configure terminal
	
###IPs

	Router(config)# interface Serial0/1/0
		Router(config-if)# ip address 172.17.12.1 255.255.255.0
		Router(config-if)# exit
		
	Router(config)# interface Serial0/1/1
		Router(config-if)# ip address 172.17.15.1 255.255.255.0
		Router(config-if)# exit
		
	Router(config)# interface GigabitEthernet0/0
		Router(config-if)# ip address 172.16.1.1 255.255.255.0
		Router(config-if)# exit
		
	AÑADIENDO UN 'no' AL PRINCIPO BORRAMOS LA IP
	 no ip address 172.16.1.1 255.255.255.0
		
###Rutas estaticas

	Router(config)#ip route 172.16.1.0 255.255.255.0 172.17.15.2
	
	AÑADIENDO UN 'no' AL PRINCIPO BORRAMOS LA RUTA
	no ip route 172.16.1.0 255.255.255.0 172.17.15.2
	
#------------------------------------------------------------------------------------

#Router1

###IPs	

	interface Serial0/1/0
	ip address 172.17.12.1 255.255.255.0
	clock rate 128000
    exit
    interface Serial0/1/1
    ip address 172.17.15.1 255.255.255.0
    clock rate 128000
    exit
    interface GigabitEthernet0/0
    ip address 172.16.1.1 255.255.255.0
    exit
    
###Rutas estaticas
    
    ip route 172.16.1.0 255.255.255.0 172.17.15.2
    ip route 172.17.15.0 255.255.255.0 172.17.15.2
    ip route 0.0.0.0 0.0.0.0 172.17.12.2
    
    

#Router2

###IPs	

	interface Serial0/1/0
	ip address 172.17.23.1 255.255.255.0
	clock rate 128000
    exit
    interface Serial0/1/1
    ip address 172.17.12.2 255.255.255.0
    clock rate 128000
    exit
    interface GigabitEthernet0/0
    ip address 172.16.2.1 255.255.255.0
    exit
    
###Rutas estaticas
    
    ip route 172.17.12.0 255.255.255.0 172.17.12.1
    ip route 172.16.2.0 255.255.255.0 172.17.12.1
    ip route 0.0.0.0 0.0.0.0 172.17.23.2
    
    
    
#Router3

###IPs	

	interface Serial0/1/0
	ip address 172.17.34.1 255.255.255.0
	clock rate 128000
    exit
    interface Serial0/1/1
    ip address 172.17.23.2 255.255.255.0
    clock rate 128000
    exit
    interface GigabitEthernet0/0
    ip address 172.16.3.1 255.255.255.0
    exit
    
###Rutas estaticas
    
    ip route 172.17.23.0 255.255.255.0 172.17.23.1
    ip route 172.16.3.0 255.255.255.0 172.17.23.1
    ip route 0.0.0.0 0.0.0.0 172.17.34.2
    
    
    
    
#Router4

###IPs	

	interface Serial0/1/0
	ip address 172.17.48.1 255.255.255.0
	clock rate 128000
    exit
    interface Serial0/1/1
    ip address 172.17.34.1 255.255.255.0
    clock rate 128000
    exit
    interface GigabitEthernet0/0
    ip address 172.16.4.1 255.255.255.0
    exit
    
###Rutas estaticas
    
    ip route 172.17.34.0 255.255.255.0 172.17.34.1
    ip route 172.16.4.0 255.255.255.0 172.17.34.1
    ip route 0.0.0.0 0.0.0.0 172.17.48.2
    
    
    
#Router5

###IPs	

	interface Serial0/1/0
	ip address 172.17.15.2 255.255.255.0
	clock rate 128000
    exit
    interface Serial0/1/1
    ip address 172.17.56.1 255.255.255.0
    clock rate 128000
    exit
    interface GigabitEthernet0/0
    ip address 172.16.5.1 255.255.255.0
    exit
    
###Rutas estaticas
    
    ip route 172.16.5.0 255.255.255.0 172.17.56.2
    ip route 172.17.56.0 255.255.255.0 172.17.56.2
    ip route 0.0.0.0 0.0.0.0 172.17.15.1
    
    

#Router6

###IPs	

	interface Serial0/1/0
	ip address 172.17.56.2 255.255.255.0
	clock rate 128000
    exit
    interface Serial0/1/1
    ip address 172.17.67.1 255.255.255.0
    clock rate 128000
    exit
    interface GigabitEthernet0/0
    ip address 172.16.6.1
    exit
    
###Rutas estaticas
    
    ip route 172.16.6.0 255.255.255.0 172.17.67.2
    ip route 172.17.67.0 255.255.255.0 172.17.67.2
    ip route 0.0.0.0 0.0.0.0 172.17.56.1
    
    
    
    
#Router7

###IPs	

	interface Serial0/1/0
	ip address 172.17.67.2 255.255.255.0
	clock rate 128000
    exit
    interface Serial0/1/1
    ip address 172.17.78.1 255.255.255.0
    clock rate 128000
    exit
    interface GigabitEthernet0/0
    ip address 172.16.7.1
	exit    
	
###Rutas estaticas
    
    ip route 172.16.8.0 255.255.255.0 172.17.78.2
    ip route 172.17.78.0 255.255.255.0 172.17.78.2
    ip route 0.0.0.0 0.0.0.0 172.17.67.1




#Router8

###IPs	

	interface Serial0/1/0
	ip address 172.17.78.2 255.255.255.0
	clock rate 128000
    exit
    interface Serial0/1/1
    ip address 172.17.48.1 255.255.255.0
    clock rate 128000
    exit
    interface GigabitEthernet0/0
    ip address 172.16.8.1
    exit
    
###Rutas estaticas
    
    ip route 172.16.8.0 255.255.255.0 172.17.48.2
    ip route 172.17.48.0 255.255.255.0 172.17.48.2
    ip route 0.0.0.0 0.0.0.0 172.17.78.1

    
    
    
    
    
